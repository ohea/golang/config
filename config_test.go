package config

import (
	"os"
	"testing"
)

type LocalConfig struct {
	Address string
	Port    int
}

func Test_getConfigDir_HOME(t *testing.T) {
	os.Unsetenv("XDG_CONFIG_HOME")
	os.Setenv("HOME", "/test")

	expected_config_dir := "/test/.config/test"
	config_dir := getConfigDir("test")

	if config_dir != expected_config_dir {
		t.Errorf("getConfigDir() returned %v, %v expected", config_dir, expected_config_dir)
	}
}

func Test_getConfigDir_XDG_CONFIG_HOME(t *testing.T) {
	os.Setenv("XDG_CONFIG_HOME", "/test/")

	expected_config_dir := "/test/test"
	config_dir := getConfigDir("test")

	if config_dir != "/test/test" {
		t.Errorf("getConfigDir() returned %v, %v expected", config_dir, expected_config_dir)
	} else {
		t.Log("Success!")
	}
}

func Test_ConfigGetConfigFile_HOME(t *testing.T) {
	os.Unsetenv("XDG_CONFIG_HOME")
	os.Setenv("HOME", "/tmp")

	expected_config_file := "/tmp/.config/test/test.toml"

	config := Config[LocalConfig]{
		Name:     "test",
		Filename: "test",
		Config:   LocalConfig{},
	}

	os.RemoveAll(getConfigDir(config.Name))

	_, err := config.Get()
	if err != nil {
		t.Errorf("Failed to get config: %v", err)
	}

	if expected_config_file != *config.ConfigFile {
		t.Errorf("config.ConfigFile is %v, %v expected", *config.ConfigFile, expected_config_file)
	} else {
		t.Log("Success!")
	}
}

func Test_ConfigGetConfigDir_XDG_CONFIG_HOME(t *testing.T) {
	os.Setenv("XDG_CONFIG_HOME", "/tmp/")

	expected_config_dir := "/tmp/test/test.toml"

	config := Config[LocalConfig]{
		Name:     "test",
		Filename: "test",
		Config:   LocalConfig{},
	}

	os.RemoveAll(getConfigDir(config.Name))

	_, err := config.Get()
	if err != nil {
		t.Errorf("Failed to get config: %v", err)
	}

	if expected_config_dir != *config.ConfigFile {
		t.Errorf("config.ConfigFile is %v, %v expected", *config.ConfigFile, expected_config_dir)
	} else {
		t.Log("Success!")
	}
}

func Test_ConfigGetReturnVal_NoConfigFile(t *testing.T) {
	os.Setenv("XDG_CONFIG_HOME", "/tmp")

	config := Config[LocalConfig]{
		Name:     "ohea-config",
		Filename: "config",
		Config:   LocalConfig{},
	}

	os.RemoveAll(getConfigDir(config.Name))

	ret, err := config.Get()
	if err != nil {
		t.Errorf("Failed to get config: %v", err)
	} else if ret {
		t.Log("Success!")
	} else if !ret {
		t.Errorf("Config.Get() returned false despite config file not existing")
	}
}

func Test_ConfigGetConfig_NoConfigFile(t *testing.T) {
	os.Setenv("XDG_CONFIG_HOME", "/tmp")

	expected_address := "127.0.0.1"
	experted_port := 9999

	config := Config[LocalConfig]{
		Name:     "ohea-config",
		Filename: "config",
		Config: LocalConfig{
			Address: expected_address,
			Port:    experted_port,
		},
	}

	os.RemoveAll(getConfigDir(config.Name))

	_, err := config.Get()
	if err != nil {
		t.Errorf("Failed to get config: %v", err)
	}

	if config.Config.Address != expected_address {
		t.Errorf("Returned value %v did not match expected value %v", config.Config.Address, expected_address)
	} else if config.Config.Port != experted_port {
		t.Errorf("Returned value %v did not match expected value %v", config.Config.Port, experted_port)
	} else {
		t.Log("Success!")
	}
}

func Test_ConfigGetConfig_ConfigFile(t *testing.T) {
	os.Setenv("XDG_CONFIG_HOME", "/tmp")

	expected_address := "127.0.0.1"
	experted_port := 9999

	config := Config[LocalConfig]{
		Name:     "ohea-config",
		Filename: "config",
		Config: LocalConfig{
			Address: expected_address,
			Port:    experted_port,
		},
	}

	os.RemoveAll(getConfigDir(config.Name))

	_, err := config.Get()
	if err != nil {
		t.Errorf("Failed to get config: %v", err)
	}

	config = Config[LocalConfig]{
		Name:     "ohea-config",
		Filename: "config",
		Config: LocalConfig{
			Address: "",
			Port:    0,
		},
	}

	_, err = config.Get()
	if err != nil {
		t.Errorf("Failed to get config: %v", err)
	}

	if config.Config.Address != expected_address {
		t.Errorf("Returned value %v did not match expected value %v", config.Config.Address, expected_address)
	} else if config.Config.Port != experted_port {
		t.Errorf("Returned value %v did not match expected value %v", config.Config.Port, experted_port)
	} else {
		t.Log("Success!")
	}

}
